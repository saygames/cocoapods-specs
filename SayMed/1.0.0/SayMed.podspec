# Copyright 2021 SayGames. All rights reserved.


Pod::Spec.new do |spec|

    spec.authors = 'SayGames'
    spec.name = 'SayMed'
    spec.version = '1.0.0'

    spec.platform = :ios
    spec.ios.deployment_target = '9.0'

    spec.summary = 'SayGames iOS SDK'
    spec.homepage = 'https://saygames.by/'
    spec.license = { :type => 'Copyright', :text => 'Copyright 2021 SayGames. All rights reserved.' }
    
    spec.source = 
    { 
        :http => 'https://sayrepository.fra1.digitaloceanspaces.com/SayMediation/1.0.0/SayMediation.zip',
        :type => 'zip'
    }
    spec.description = <<-DESC
        We're mobile game publisher. Our forte is establishing sustainable relationship with world-class development teams.
    DESC
  

    spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    spec.vendored_frameworks = 'SayMediation/SayMed.framework'
  
  end